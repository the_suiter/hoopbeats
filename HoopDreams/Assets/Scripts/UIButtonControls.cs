﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIButtonControls : MonoBehaviour
{

    public Text buttonText;
    public GameObject ball;
    public float spin = .5f;
    public bool mouseOnButton;
    public string pickScene;

    // Start is called before the first frame update
    void Start()
    {
        mouseOnButton = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (mouseOnButton)
        {
            ball.transform.Rotate(new Vector3(0, 0, spin));
        }
    }

    void OnMouseEnter()
    {
        buttonText.fontSize = 200;
        Debug.Log("Missed");
        mouseOnButton = true;
    }

    void OnMouseExit()
    {
        buttonText.fontSize = 150;
        mouseOnButton = false;
    }
    public void SceneChange()
	{
        SceneManager.LoadScene(pickScene);
	}
    public void OnMouseDown()
	{
        if (mouseOnButton)
        {
            SceneChange();
        }
	}
}
