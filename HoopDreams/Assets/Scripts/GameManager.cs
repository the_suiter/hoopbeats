﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public AudioSource theMusic;

    public bool startPlaying;

    public BeatScroller theBS;

    public static GameManager instance;

    public int currentScore;
    public int scorePerNote = 1;
    public int scorePerGoodNote = 2;
    public int scorePerPerfectNote = 3;
    public int aiScore;
    public int aiScorePerNote = 2;
    public int aiScorePerGoodNote = 2;
    public int aiScorePerPerfectNote = 1;
    public int aiScorePerMiss = 3;

    public int currentMultiplier;
    public int multiplierTracker;
    public int[] multiplierThresholds;

    public float countdownTimer = 3.0f;

    public Text scoreText;
    public Text multiText;
    public Text aiScoreText;
    public Text startText;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        scoreText.text = "0";
        currentMultiplier = 1;
        aiScoreText.text = "0";
        startText.text = "Ready?";
    }

    // Update is called once per frame
    void Update()
    {
		if (!startPlaying)
		{
            countdownTimer -= Time.deltaTime;
            startText.text = (countdownTimer).ToString("0");
            
            theBS.hasStarted = true;


            if (countdownTimer <1)
			{
                startText.text = "GO!";

            }
            if(countdownTimer < 0)

            {
                startPlaying = true;

                theMusic.Play();

            }
        }

        if (startPlaying)
		{
            startText.text = "";
        }
    }

    public void NoteHit()
	{
        

        if (currentMultiplier - 1 < multiplierThresholds.Length)
        {
            multiplierTracker++;

            if (multiplierThresholds[currentMultiplier - 1] <= multiplierTracker)
            {
                multiplierTracker = 0;
                currentMultiplier++;
            }
        }

        //multiText.text = "Multiplier x" + currentMultiplier;

        //currentScore += scorePerNote * currentMultiplier;
        scoreText.text = "" + currentScore;
        aiScoreText.text = "" + aiScore;
    }

    public void NormalHit()
	{
        Debug.Log("Hit");
        currentScore += scorePerNote; //* currentMultiplier;
        aiScore += aiScorePerNote;
        NoteHit();
	}

    public void GoodHit()
	{
        Debug.Log("Good Hit");
        currentScore += scorePerGoodNote; //* currentMultiplier;
        aiScore += aiScorePerGoodNote;
        NoteHit();
    }

    public void PerfectHit()
    {
        Debug.Log("Perfect Hit");
        currentScore += scorePerPerfectNote; //* currentMultiplier;
        aiScore += aiScorePerNote;
        NoteHit();
    }

    public void NoteMissed()
	{
        Debug.Log("Missed");
        currentMultiplier = 1;
        multiplierTracker = 0;
        multiText.text = "Multiplier x" + currentMultiplier;
        aiScore += aiScorePerMiss;
        aiScoreText.text = "" + aiScore;
    }
}
