﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteObject : MonoBehaviour
{
    public bool canBePressed;

    public KeyCode keyToPress;

    public bool hit;

    public GameObject HitEffect;
    public GameObject GoodEffect;
    public GameObject PerfectEffect;
    public GameObject MissEffect;

    public SpriteRenderer spriteRenderer;


    // Start is called before the first frame update
    void Start()
    {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(keyToPress))
		{
			if (canBePressed)
			{
                hit = true;

                gameObject.SetActive(false);

                //GameManager.instance.NoteHit();

                if(Mathf.Abs(transform.position.y) > 0.25)
				{
                    GameManager.instance.NormalHit();
                    Instantiate(HitEffect, transform.position, HitEffect.transform.rotation);
				}
                else if (Mathf.Abs(transform.position.y) > 0.05)
				{
                    GameManager.instance.GoodHit();
                    Instantiate(GoodEffect, transform.position, GoodEffect.transform.rotation);
                }
				else
				{
                    GameManager.instance.PerfectHit();
                    Instantiate(PerfectEffect, transform.position, PerfectEffect.transform.rotation);
                }
			}
		}
    }


    private void OnTriggerEnter2D(Collider2D other)
	{
        if(other.tag == "Activator")
		{
            canBePressed = true;
		}
        if (other.tag == "on")
        {
            this.spriteRenderer.enabled = true;
            Debug.Log("vis"); 
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!hit)
        {


            if (other.tag == "Activator")
            {
                canBePressed = false;

                GameManager.instance.NoteMissed();
                Instantiate(MissEffect, transform.position, MissEffect.transform.rotation);
            }
        }
    }
}
