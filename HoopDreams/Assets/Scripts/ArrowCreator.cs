﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowCreator : MonoBehaviour
{

    public GameObject arrowPick;

    public int DelayMin;
    public int DelayMax;

    public float spawnTime;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnArrow());
    }

    // Update is called once per frame
    void Update()
    {
        spawnTime = Random.Range(DelayMin, DelayMax);
    }

    IEnumerator spawnArrow()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnTime);
            Instantiate(arrowPick, transform.position, transform.rotation);
        }
    }
}
